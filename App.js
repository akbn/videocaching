import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import Video from "react-native-video";

const VIDEO_URL =
  "https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_30mb.mp4";
const AUDIO_URL = "https://s3-us-west-2.amazonaws.com/bnirvana-tmp/test.m4a";

type Props = {};
export default class App extends Component<Props> {
  state = {
    status: "Loading App"
  };

  onLoad = () => {
    this.setState({ status: "Loading File" });
  };

  onProgress = params => {
    const { seekableDuration, playableDuration } = params;
    if (seekableDuration === playableDuration) {
      this.setState({ status: "Download complete. Time to save the file." });
    } else {
      const percent = parseInt((playableDuration * 100) / seekableDuration, 10);
      this.setState({ status: `Download Progress ${percent}%` });
    }
  };

  render() {
    const { status } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{status}</Text>
        <Video
          source={{ uri: VIDEO_URL }}
          ref={ref => {
            this.player = ref;
          }}
          onLoad={this.onLoad}
          onProgress={this.onProgress}
          style={styles.backgroundVideo}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  text: {
    position: "absolute",
    top: 100
  },
  backgroundVideo: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
});
